// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

// This is a simple proof-of-concept for generating external schemas for an EventTaxonomy.
//
package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

func main() {
	flag.Parse()

	if len(os.Args) <= 1 {
		log.Fatal("give taxonomy json file as only parameter\n")
	}
	file, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatalf("Error while reading %s: %v\n", os.Args[1], err)
	}
	var taxonomy model.EventTaxonomy
	err = json.Unmarshal([]byte(file), &taxonomy)
	if err != nil {
		log.Fatalf("Error while parsing JSON from %s: %v\n", os.Args[1], err)
	}
	schema, err := makeCapnpSchema(&taxonomy)
	if err != nil {
		log.Fatalf("Error while generating Cap'n Proto schema: %v\n", err)
	}
	fmt.Print(schema)
}

func shouldIncludeLifecycle(lifecycle model.Lifecycle) bool {
	return lifecycle == model.ProductionLifecycle
}

func capnTypeString(pt model.Type) string {
	ret := ""
	if model.IsArrayType(pt) {
		ret += "List("
	}
	ret += capnBasicTypeString(pt.ScalarType())
	if model.IsArrayType(pt) {
		ret += ")"
	}
	return ret
}

func capnBasicTypeString(typeStr string) string {
	switch typeStr {
	case "Date":
		return "Text"
	case "String":
		return "Text"
	case "Int8":
		return "Int8"
	case "Int16":
		return "Int16"
	case "Int32":
		return "Int32"
	case "Int64":
		return "Int64"
	case "Float32":
		return "Float32"
	case "Float64":
		return "Float64"
	case "Bool":
		return "Bool"
	default:
		return ""
	}
}

func appIDToStructName(appID string) string {
	return strcase.ToCamel(strings.ReplaceAll(appID, ".", "_"))
}

func propertyNameToCapnpName(name string) string {
	return strcase.ToLowerCamel(name)
}

func makeCapnpSchemaWithID(taxonomy *model.EventTaxonomy, id string) (string, error) {
	var buffer strings.Builder
	buffer.WriteString("@0x")
	buffer.WriteString(id)
	buffer.WriteString(";\n\nstruct ")
	buffer.WriteString(appIDToStructName(taxonomy.AppID))
	buffer.WriteString("Taxonomy {\n")
	num := 0
	for _, p := range taxonomy.Properties {
		if !shouldIncludeLifecycle(p.Lifecycle) {
			continue
		}
		pt, err := model.ParseType(p.Type)
		if err != nil {
			log.Fatal(err)
		}
		cpType := capnTypeString(pt)
		if cpType == "" {
			continue
		}
		buffer.WriteString("    ")
		buffer.WriteString(propertyNameToCapnpName(p.Name))
		buffer.WriteString(fmt.Sprintf(" @%d :", num))
		buffer.WriteString(cpType)
		buffer.WriteString(";\n")
		num++
	}
	buffer.WriteString("}\n")

	return buffer.String(), nil
}

func makeCapnpSchema(taxonomy *model.EventTaxonomy) (string, error) {
	hasher := md5.New()
	hasher.Write([]byte(taxonomy.AppID))
	return makeCapnpSchemaWithID(taxonomy, hex.EncodeToString(hasher.Sum(nil))[:16])
}
