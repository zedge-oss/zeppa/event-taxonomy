// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package main

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

var _ = When("generating a schema", func() {
	It("should match the expected output", func() {
		schema, err := makeCapnpSchema(taxonomy1)
		Expect(err).To(Succeed())
		Expect(schema).To(Equal(capnp1))
	})
})

var taxonomy1 = &model.EventTaxonomy{
	AppID: AppID1,
	Properties: []*model.EventProperty{
		makeProperty("str", "String"),
		makePropertyWithLifecycle("preProdStr", "String", model.PreProductionLifecycle),
		makeProperty("date", "Date"),
		makeProperty("number_with_8_bits", "Int8"),
		makeProperty("i16", "Int16"),
		makeProperty("i32", "Int32"),
		makeProperty("i64", "Int64"),
		makeProperty("f32", "Float32"),
		makeProperty("f64", "Float64"),
		makeProperty("str_array", "Array(String)"),
	},
}

func makePropertyWithLifecycle(name, typ string, lifecycle model.Lifecycle) *model.EventProperty {
	return &model.EventProperty{Name: name, Scope: model.EventScope, Lifecycle: lifecycle, Type: typ}
}

func makeProperty(name, typ string) *model.EventProperty {
	return makePropertyWithLifecycle(name, typ, model.ProductionLifecycle)
}

const (
	AppID1 = "test1"
	capnp1 = `@0x5a105e8b9d40e132;

struct Test1Taxonomy {
    str @0 :Text;
    date @1 :Text;
    numberWith8Bits @2 :Int8;
    i16 @3 :Int16;
    i32 @4 :Int32;
    i64 @5 :Int64;
    f32 @6 :Float32;
    f64 @7 :Float64;
    strArray @8 :List(Text);
}
`
)

func TestCapnProtoGenerator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecsWithDefaultAndCustomReporters(t, "Cap'n Proto Model", []Reporter{
		reporters.NewJUnitReporter("junit.xml"),
	})
}
