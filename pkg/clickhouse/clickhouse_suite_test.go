// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"errors"
	"fmt"
	"io/ioutil"
	golog "log"
	"net"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/ory/dockertest"
	"github.com/stretchr/testify/require"
)

const (
	clickhouseTestImage    = "yandex/clickhouse-server"
	clickhouseTestImageTag = "19.13"
	zookeeperTestImage     = "gcr.io/google_samples/k8szk"
	zookeeperTestImageTag  = "v3"
)

var (
	dockerConn    *dockertest.Pool
	defaultImages = map[string]string{
		"CLICKHOUSE": clickhouseTestImage,
		"ZOOKEEPER":  zookeeperTestImage,
	}
	defaultImageTags = map[string]string{
		"CLICKHOUSE": clickhouseTestImageTag,
		"ZOOKEEPER":  zookeeperTestImageTag,
	}
)

func TestMain(m *testing.M) {
	var err error
	dockerConn, err = dockertest.NewPool("")
	if err != nil {
		golog.Fatalf("NewPool(): %v\n", err)
	}
	code := m.Run()
	os.Exit(code)
}

type dockerContainerMainFunc func(*dockertest.Pool, *dockertest.Resource, *StatefulDB)

func runWithClickhouseContainer(t *testing.T, mainFunc dockerContainerMainFunc) {
	zkContainer, err := dockerConn.RunWithOptions(&dockertest.RunOptions{
		Repository: testImage("ZOOKEEPER"),
		Tag:        testImageTag("ZOOKEEPER"),
		Hostname:   "zookeeper-0",
		Env:        []string{"ZK_REPLICAS=1"},
		Cmd:        []string{"sh", "-c", "zkGenConfig.sh && exec zkServer.sh start-foreground"},
		//NetworkID:    dockerNet.ID,
		ExposedPorts: []string{"2181/tcp"},
	})
	require.NoError(t, err)
	defer func() { _ = dockerConn.Purge(zkContainer) }()
	require.NoError(t, dockerConn.Retry(func() error {
		addr := zkContainer.GetHostPort("2181/tcp")
		if conn, err := net.DialTimeout("tcp", addr, 2*time.Second); err == nil {
			if _, err = conn.Write([]byte("ruok")); err == nil {
				if resp, err := ioutil.ReadAll(conn); err == nil {
					if string(resp) != "imok" {
						return errors.New("zookeeper not ok")
					}
				}
			}
		}
		return err
	}))
	chConfigDir, err := filepath.Abs(filepath.Join("..", "..", "testdata", "clickhouse", "config"))
	require.NoError(t, err)
	chContainer, err := dockerConn.RunWithOptions(&dockertest.RunOptions{
		Repository:   testImage("CLICKHOUSE"),
		Tag:          testImageTag("CLICKHOUSE"),
		Hostname:     "clickhouse",
		Mounts:       []string{chConfigDir + ":/etc/clickhouse-server"},
		ExtraHosts:   []string{"zookeeper-0:" + zkContainer.GetBoundIP("2181/tcp")},
		ExposedPorts: []string{"9000/tcp"},
	})
	require.NoError(t, err)
	defer func() { _ = dockerConn.Purge(chContainer) }()
	db := NewLoggingStatefulDB()
	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	require.NoError(t, dockerConn.Retry(func() error {
		dsn := fmt.Sprintf("tcp://%s?debug=false", getHostPort(chContainer, "9000/tcp"))
		if _, err := db.OpenDB(dsn); err != nil {
			return err
		}
		return nil
	}))
	mainFunc(dockerConn, chContainer, db)
}

func testImage(service string) string {
	image := os.Getenv("TEST_" + service + "_IMAGE")
	if image == "" {
		image = defaultImages[service]
	}
	return image
}

func testImageTag(service string) string {
	tag := os.Getenv("TEST_" + service + "_IMAGE_TAG")
	if tag == "" {
		tag = defaultImageTags[service]
	}
	return tag
}

func getHostPort(container *dockertest.Resource, portID string) string {
	ip := container.GetBoundIP(portID)
	if ip == "0.0.0.0" && os.Getenv("TEST_DOCKER_ADDR") != "" {
		ip = os.Getenv("TEST_DOCKER_ADDR")
	}
	port := container.GetPort(portID)
	return net.JoinHostPort(ip, port)
}
