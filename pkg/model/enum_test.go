// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("For enums,", func() {
	var (
		enum EventEnum = EventEnum{Name: "MyEnum"}
	)
	When("an enum has two fields with the same value", func() {
		BeforeEach(func() {
			enum.Fields = []*EventEnumField{
				{Name: "FIELD1", Value: 1},
				{Name: "FIELD2", Value: 1},
			}
		})
		It("should produce an error", func() {
			Expect(enum.OK()).To(MatchError(`re-used value (1) in fields "FIELD1" and "FIELD2"`))
		})
	})
	When("an enum has two fields with the same name", func() {
		BeforeEach(func() {
			enum.Fields = []*EventEnumField{
				{Name: "FIELD1", Value: 1},
				{Name: "FIELD1", Value: 2},
			}
		})
		It("should produce an error", func() {
			Expect(enum.OK()).To(MatchError(`duplicate field name: "FIELD1"`))
		})
	})
	When("an enum has a field with a value that is outside [-32768, 32767]", func() {
		BeforeEach(func() {
			enum.Fields = []*EventEnumField{{Name: "FIELD1", Value: 65535}}
		})
		It("should produce an error", func() {
			Expect(enum.OK()).To(MatchError(`field "FIELD1": value (65535) must be in the range [-32768, 32767]`))
		})
	})
	When("an enum has an invalid name", func() {
		BeforeEach(func() {
			enum.Name = "-BadName!"
		})
		It("should produce an error", func() {
			Expect(enum.OK()).To(MatchError(MatchRegexp(`^name must match`)))
		})
	})
	When("getting the max enum field", func() {
		BeforeEach(func() {
			enum.Fields = []*EventEnumField{
				{Value: 1, Lifecycle: ProductionLifecycle},
				{Value: 2, Lifecycle: PreProductionLifecycle},
				{Value: 3, Lifecycle: ProductionEndOfLifeLifecycle},
				{Value: 4, Lifecycle: ProductionEndOfLifeLifecycle},
			}
		})
		Context("there are no `Void` fields", func() {
			It("should return the highest value", func() {
				Expect(enum.MaxValue()).To(Equal(4))
			})
		})
		Context("there is a `Void` field", func() {
			BeforeEach(func() {
				enum.Fields[3].Lifecycle = VoidLifecycle
			})
			It("should return the highest value but disregard the `Void` field", func() {
				Expect(enum.MaxValue()).To(Equal(3))
			})
		})
		Context("enum fields are not defined", func() {
			BeforeEach(func() {
				enum.Fields = nil
			})
			It("should return 0", func() {
				Expect(enum.MaxValue()).To(Equal(0))
			})
		})
		Context("the array of fields is empty", func() {
			BeforeEach(func() {
				enum.Fields = []*EventEnumField{}
			})
			It("should return 0", func() {
				Expect(enum.MaxValue()).To(Equal(0))
			})
		})
	})

	When("getting the next enum field", func() {
		BeforeEach(func() {
			enum.Fields = []*EventEnumField{
				{Value: 1, Lifecycle: ProductionLifecycle},
				{Value: 2, Lifecycle: PreProductionLifecycle},
				{Value: 3, Lifecycle: ProductionEndOfLifeLifecycle},
				{Value: 5, Lifecycle: ProductionEndOfLifeLifecycle},
			}
		})
		Context("there are no `Void` fields", func() {
			It("should return the lowest available value", func() {
				Expect(enum.NextValue()).To(Equal(4))
			})
		})
		Context("there is a `Void` field", func() {
			BeforeEach(func() {
				enum.Fields[1].Lifecycle = VoidLifecycle
			})
			It("should consider the `Void` field not available", func() {
				Expect(enum.NextValue()).To(Equal(4)) // Reference: https://gitlab.com/zedge-oss/zeppa/taxman/-/merge_requests/6
			})
		})
	})
	When("getting the first enum field", func() {
		Context("enum fields are not defined", func() {
			BeforeEach(func() {
				enum.Fields = nil
			})
			It("should return 1", func() {
				Expect(enum.NextValue()).To(Equal(1))
			})
		})
		Context("the array of fields is empty", func() {
			BeforeEach(func() {
				enum.Fields = []*EventEnumField{}
			})
			It("should return 1", func() {
				Expect(enum.NextValue()).To(Equal(1))
			})
		})
	})
})
