/*
 * Copyright 2018-2020 Zedge, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package model

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Computing changes", func() {
	When("A taxonomy is updated", func() {
		var changes *TaxonomyUpdate
		var err error
		It("should find the updated enums and properties", func() {
			changes, err = ComputeChangesFromFiles("testdata/old1.json", "testdata/new1.json")
			Expect(err).ToNot(HaveOccurred())
			Expect(changes.AppID).To(BeNil())
			Expect(changes.AddedEnums).To(HaveLen(1))
			Expect(changes.AddedProperties).To(HaveLen(1))
			Expect(changes.UpdatedEnums).To(HaveLen(0))
			Expect(changes.UpdatedProperties).To(HaveLen(1))
			Expect(changes.RemovedEnums).To(HaveLen(0))
			Expect(changes.RemovedProperties).To(HaveLen(0))
			Expect(changes.AddedEnums[0].Name).To(Equal("Event"))
			Expect(changes.UpdatedProperties[0].From.Name).To(Equal("userid"))
		})
	})
})
