// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var TIMESTAMP1 = parseTime("2018-01-01T00:00:00Z")
var TIMESTAMP2 = parseTime("2019-07-01T00:00:00Z")

func parseTime(rfc3339Time string) metav1.Time {
	tmp, _ := time.Parse(time.RFC3339, rfc3339Time)
	return metav1.NewTime(tmp)
}

var _ = Describe("Validating changes,", func() {
	When("trying to remove a production property", func() {
		changes := newTaxonomyUpdate()
		changes.RemovedProperties = append(changes.RemovedProperties, &EventProperty{
			Name:      "removed_prop",
			Lifecycle: ProductionLifecycle,
		})
		It("should fail", func() {
			issues := changes.Validate()
			Expect(issues).To(HaveLen(1))
			Expect(issues[0]).To(MatchError(MatchRegexp(`^property "removed_prop": can not remove when lifecycle is `)))
		})
	})
	When("trying to change an Internal or Envelope property", func() {
		It("should fail", func() {
			// Changing anything in internal/envelope properties is not allowed
			upd := PropertyUpdate{
				From: &EventProperty{Scope: InternalScope, LastModified: TIMESTAMP1},
				To:   &EventProperty{Scope: InternalScope, LastModified: TIMESTAMP2},
			}
			issues := upd.DetectIssues([]string{})
			Expect(issues).To(HaveLen(1))
			Expect(issues[0]).To(MatchError("not allowed to change envelope or internal properties"))
		})
	})
})
