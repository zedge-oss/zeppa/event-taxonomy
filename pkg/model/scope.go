// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"encoding/json"
	"fmt"
	"strings"
)

// PropertyScope describes whether a property describes something about the event, the user
// or the event envelope.
type PropertyScope string

const (
	// EventScope - property is part of the event
	EventScope PropertyScope = "Event"
	// UserScope - property describes the user
	UserScope PropertyScope = "User"
	// EnvelopeScope - property describe the event envelope
	EnvelopeScope PropertyScope = "Envelope"
	// InternalScope - property is internal, clients are not allowed to define it
	InternalScope PropertyScope = "Internal"
)

var stringToScope = map[string]PropertyScope{
	"event":    EventScope,
	"user":     UserScope,
	"envelope": EnvelopeScope,
	"internal": InternalScope,
}

// // MarshalJSON marshals the scope as a quoted json string
// func (s *PropertyScope) MarshalJSON() ([]byte, error) {
// 	buffer := bytes.NewBufferString(`"`)
// 	buffer.WriteString(string(*s))
// 	buffer.WriteString(`"`)
// 	return buffer.Bytes(), nil
// }

// UnmarshalJSON unmashals a quoted json string to a PropertyScope value
func (s *PropertyScope) UnmarshalJSON(b []byte) error {
	var j string
	err := json.Unmarshal(b, &j)
	if err != nil {
		return err
	}
	val, ok := stringToScope[strings.ToLower(j)]
	if !ok {
		return fmt.Errorf("invalid scope: \"%s\"", j)
	}
	*s = val
	return nil
}

func (s PropertyScope) IsSacred() bool {
	return s == InternalScope || s == EnvelopeScope
}
