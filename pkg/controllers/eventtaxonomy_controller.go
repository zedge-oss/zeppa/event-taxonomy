/*
Copyright 2018 Zedge, Inc..

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/json"
	"reflect"
	"sort"
	"strings"

	"github.com/go-logr/logr"

	dwhv1beta1 "gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/clickhouse"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const taxonomyConfigMapKey = "taxonomy.json"
const fatTaxonomyConfigMapKeySuffix = "-event-taxonomy.json"

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func (r *EventTaxonomyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&dwhv1beta1.EventTaxonomy{}).
		Watches(&source.Kind{Type: &corev1.ConfigMap{}}, &handler.EnqueueRequestForOwner{
			IsController: false,
			OwnerType:    &dwhv1beta1.EventTaxonomy{},
		}).
		Complete(r)
}

var _ reconcile.Reconciler = &EventTaxonomyReconciler{}

// EventTaxonomyReconciler reconciles an EventTaxonomy object
type EventTaxonomyReconciler struct {
	client.Client
	Log        logr.Logger
	Scheme     *runtime.Scheme
	StatefulDB *clickhouse.StatefulDB
	//Recorder record.EventRecorder
}

// Reconcile reads that state of the cluster for an EventTaxonomy object and makes changes based on the state read
// and what is in the EventTaxonomy.Spec
// Automatically generate RBAC rules to allow the Controller to read EventTaxonomy objects
// +kubebuilder:rbac:groups=dwh.zedge.io,resources=eventtaxonomies,verbs=get;list;watch;create;update;patch;delete
func (r *EventTaxonomyReconciler) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	log := r.Log
	instance := &dwhv1beta1.EventTaxonomy{}
	// Fetch the EventTaxonomy instance
	var requeue bool
	var err error
	err = r.Get(context.Background(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.

			// But we want to update the fat config map.
			if requeue, err = r.createOrUpdateFatConfigMap(&request, instance, log); err != nil {
				return reconcile.Result{}, err
			}
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	if requeue, err = r.createOrUpdateConfigMap(&request, instance, log); err != nil {
		return reconcile.Result{Requeue: requeue}, err
	}

	if requeue, err = r.createOrUpdateFatConfigMap(&request, instance, log); err != nil {
		return reconcile.Result{Requeue: requeue}, err
	}

	if instance.Spec.Clickhouse == nil {
		log.Info("spec.clickhouse not present for eventtaxonomy", "name", request.NamespacedName)
	} else {
		if requeue, err := r.reconcileTableStructure(&instance.Spec, log); err != nil {
			return reconcile.Result{Requeue: requeue}, err
		}
	}
	return reconcile.Result{}, err
}

// Generates a config map named event-taxonomies which contains all taxonomies.
func (r *EventTaxonomyReconciler) createOrUpdateFatConfigMap(request *reconcile.Request, instance *dwhv1beta1.EventTaxonomy, log logr.Logger) (requeue bool, err error) {
	ns := request.Namespace
	taxonomies := &dwhv1beta1.EventTaxonomyList{}
	if err := r.Client.List(context.Background(), taxonomies, &client.ListOptions{Namespace: ns}); err != nil {
		return true, err
	}

	cm := &corev1.ConfigMap{}
	cmName := "event-taxonomies"

	err = r.Get(context.Background(), types.NamespacedName{Name: cmName, Namespace: ns}, cm)
	create := false
	if err != nil {
		if errors.IsNotFound(err) {
			cm.ObjectMeta = metav1.ObjectMeta{Name: cmName, Namespace: request.Namespace}
			cm.Data = make(map[string]string)

			for _, taxonomy := range taxonomies.Items {
				cm.Data[taxonomy.Name+fatTaxonomyConfigMapKeySuffix] = ""
				if taxonomy.Name != taxonomy.Spec.Taxonomy.AppID {
					cm.Data[taxonomy.Spec.Taxonomy.AppID+fatTaxonomyConfigMapKeySuffix] = ""
				}
			}
			create = true
		} else {
			return false, err
		}
	}

	oldTaxJSON := cm.DeepCopy().Data
	cm.Data = make(map[string]string)

	for _, taxonomy := range taxonomies.Items {

		if taxonomy.DeletionTimestamp.IsZero() {
			cmBytes, err := json.MarshalIndent(taxonomy.Spec.Taxonomy, "", "  ")
			if err != nil {
				return false, err
			}
			cm.Data[taxonomy.Name+fatTaxonomyConfigMapKeySuffix] = string(cmBytes)
			if taxonomy.Name != taxonomy.Spec.Taxonomy.AppID {
				cm.Data[taxonomy.Spec.Taxonomy.AppID+fatTaxonomyConfigMapKeySuffix] = string(cmBytes)
			}

			if err = AddControllerReference(&taxonomy, cm, r.Scheme); err != nil {
				return false, err
			}
		}
	}
	newTaxJSON := cm.Data
	if create {
		log.Info("Creating ConfigMap", "name", cm.Name, "namespace", cm.Namespace)
		err = r.Create(context.Background(), cm)
	} else if !reflect.DeepEqual(oldTaxJSON, newTaxJSON) {
		log.Info("Updating ConfigMap", "name", cm.Name, "namespace", cm.Namespace)
		err = r.Update(context.Background(), cm)
	}
	return false, nil
}

func (r *EventTaxonomyReconciler) createOrUpdateConfigMap(request *reconcile.Request, instance *dwhv1beta1.EventTaxonomy, log logr.Logger) (requeue bool, err error) {
	cm := &corev1.ConfigMap{}
	cmName := request.Name + "-event-taxonomy"
	ns := request.Namespace
	err = r.Get(context.Background(), types.NamespacedName{Name: cmName, Namespace: ns}, cm)
	create := false
	if err != nil {
		if errors.IsNotFound(err) {
			cm.ObjectMeta = metav1.ObjectMeta{Name: cmName, Namespace: request.Namespace}
			cm.Data = make(map[string]string)
			cm.Data[taxonomyConfigMapKey] = ""
			if err = controllerutil.SetControllerReference(instance, cm, r.Scheme); err != nil {
				return false, err
			}
			create = true
		} else {
			return false, err
		}
	}
	oldTaxJSON := cm.Data[taxonomyConfigMapKey]
	cmBytes, err := json.MarshalIndent(instance.Spec.Taxonomy, "", "  ")
	if err != nil {
		return false, err
	}
	newTaxJSON := string(cmBytes)
	cm.Data[taxonomyConfigMapKey] = newTaxJSON
	if create {
		log.Info("Creating ConfigMap", "name", cm.Name, "namespace", cm.Namespace)
		err = r.Create(context.Background(), cm)
	} else if oldTaxJSON != newTaxJSON {
		log.Info("Updating ConfigMap", "name", cm.Name, "namespace", cm.Namespace)
		err = r.Update(context.Background(), cm)
	}
	return false, nil
}

// Returns whether the reconcile should be requeued and an error.
func (r *EventTaxonomyReconciler) reconcileTableStructure(spec *dwhv1beta1.EventTaxonomySpec, log logr.Logger) (bool, error) {
	chSpec := spec.Clickhouse
	db, err := r.StatefulDB.OpenDB(chSpec.DSN)
	if err != nil {
		log.Error(err, "Failed connecting to Clickhouse", "dsn", chSpec.DSN)
		return true, err
	}
	defer func() { _ = db.Close() }()
	errorAndRequeue := func(err error) (bool, error) { return true, err }
	errorAndNoRequeue := func(err error) (bool, error) { return false, err }
	sortDistributedFirst(chSpec.TableBindings)
	for _, binding := range chSpec.TableBindings {
		clusterName := binding.Table.ClusterName
		if clusterName == "" {
			clusterName = chSpec.ClusterName
		}
		tableName := binding.Table.Name
		databaseName := binding.Table.DatabaseName
		table, err := binding.Table.Inspect(r.StatefulDB)
		if err != nil {
			return errorAndNoRequeue(err)
		}
		log.V(1).Info("Current structure", "database", databaseName, "table", tableName, "structure", table)
		spec.Taxonomy.BuildIndex()
		// baseCols contains column names that we should not touch
		baseCols := make(map[string]bool, len(binding.Table.Columns)+len(chSpec.BaseColumns))
		for _, column := range binding.Table.Columns {
			baseCols[column.Name] = true
		}
		for _, column := range chSpec.BaseColumns {
			baseCols[column.Name] = true
		}
		oldCols := make(map[string]*clickhouse.Column)
		for _, column := range table.Columns {
			if !baseCols[column.Name] {
				oldCols[column.Name] = column
			}
		}
		newCols := make(map[string]*clickhouse.Column)
		for _, column := range EventPropertiesAsColumns(&spec.Taxonomy, binding) {
			if !baseCols[column.Name] {
				newCols[column.Name] = column
			}
		}
		alteredCol := make(map[string]*clickhouse.Column)
		dropped := make([]string, 0)
		added := make(map[string]*clickhouse.Column, 0)
		log.V(2).Info("DEBUG", "oldCols", oldCols, "newCols", newCols)
		for colName, oldCol := range oldCols {
			newCol, stillExists := newCols[colName]
			if stillExists {
				if !oldCol.EquivalentTo(*newCol, r.StatefulDB) {
					alteredCol[colName] = newCol
					log.V(0).Info("Modifying column", "database", table.DatabaseName, "table", table.Name, "oldCol", oldCol, "newCol", newCol)
					if err = table.ModifyColumn(newCol, r.StatefulDB); err != nil {
						return errorAndNoRequeue(err)
					}
				}
			} else {
				dropped = append(dropped, colName)
				log.V(0).Info("Deleting column", "database", table.DatabaseName, "table", table.Name, "column", colName)
				if err = table.DropColumn(oldCol, r.StatefulDB); err != nil {
					return errorAndRequeue(err)
				}
			}
		}
		for colName, newCol := range newCols {
			if _, existedBefore := oldCols[colName]; !existedBefore {
				added[colName] = newCol
				log.V(0).Info("Adding column", "database", table.DatabaseName, "table", table.Name, "column", newCol)
				if err = table.AddColumn(newCol, r.StatefulDB); err != nil {
					return errorAndRequeue(err)
				}
			}
		}
	}
	return false, nil
}

func sortDistributedFirst(tables []dwhv1beta1.ClickhouseTableBinding) {
	sort.Slice(tables[:], func(i, j int) bool {
		return strings.HasPrefix(tables[i].Table.Engine, "Distributed")
	})
}

// EventPropertiesAsColumns returns a list of columns from the properties of an EventTaxonomy
func EventPropertiesAsColumns(taxonomy *model.EventTaxonomy, binding dwhv1beta1.ClickhouseTableBinding) []*clickhouse.Column {
	lifecycles := binding.Lifecycles
	cols := make([]*clickhouse.Column, 0)
	lcMap := make(map[model.Lifecycle]bool, len(lifecycles))
	for _, lc := range lifecycles {
		lcMap[lc] = true
	}
	for _, prop := range taxonomy.Properties {
		if _, include := lcMap[prop.Lifecycle]; include {
			col := &clickhouse.Column{
				Name: prop.Name,
				Type: clickhouse.AsClickhouseType(taxonomy, prop.Type),
			}
			if ttlOverride, found := binding.ColumnTTLOverrides[prop.Name]; found {
				col.TTL = ttlOverride
			}
			if codecOverride, found := binding.ColumnCodecOverrides[prop.Name]; found {
				col.Codec = codecOverride
			}
			cols = append(cols, col)
		}
	}
	return cols
}
