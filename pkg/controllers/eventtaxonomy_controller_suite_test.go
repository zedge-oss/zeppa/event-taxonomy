/*
Copyright 2018 Zedge, Inc..

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"log"
	"os"
	"path/filepath"
	"sync"
	"testing"

	. "github.com/onsi/gomega"
	"github.com/ory/dockertest/docker/pkg/ioutils"
	uberzap "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"sigs.k8s.io/controller-runtime/pkg/manager"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/clickhouse"
)

var cfg *rest.Config

func TestMain(m *testing.M) {
	t := &envtest.Environment{
		CRDDirectoryPaths: []string{filepath.Join("..", "..", "config", "crd", "bases")},
	}
	if os.Getenv("USE_EXISTING_CLUSTER") == "true" {
		useExistingCluster := true
		t.UseExistingCluster = &useExistingCluster
	}
	apis.AddToScheme(scheme.Scheme)
	level := uberzap.NewAtomicLevelAt(zapcore.DebugLevel)
	logf.SetLogger(zap.New(func(o *zap.Options) {
		o.Development = true
		o.DestWritter = &ioutils.NopWriter{}
		o.Level = &level
	}))

	var err error
	if cfg, err = t.Start(); err != nil {
		log.Fatal(err)
	}

	code := m.Run()
	t.Stop()
	os.Exit(code)
}

// SetupTestReconcile returns a reconcile.Reconcile implementation that delegates to inner and
// writes the request to requests after Reconcile is finished.
func SetupEventTaxonomyReconcilerTest() (context.Context, error) {
	mgr, err := ctrl.NewManager(cfg, ctrl.Options{MetricsBindAddress: ":8911"})
	if err != nil {
		return nil, err
	}

	err = (&EventTaxonomyReconciler{
		Client:     mgr.GetClient(),
		Scheme:     scheme.Scheme,
		Log:        logf.Log,
		StatefulDB: clickhouse.NewLoggingStatefulDB(),
		//Recorder: mgr.GetEventRecorderFor("experiment-controller"),
	}).SetupWithManager(mgr)
	if err != nil {
		return nil, err
	}
	return context.Background(), nil
}

// StartTestManager adds recFn
func StartTestManager(mgr manager.Manager, g *GomegaWithT) (chan struct{}, *sync.WaitGroup) {
	stop := make(chan struct{})
	wg := &sync.WaitGroup{}
	go func() {
		wg.Add(1)
		g.Expect(mgr.Start(stop)).To(Succeed())
		wg.Done()
	}()
	return stop, wg
}
